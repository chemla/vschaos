from ..data_generic import Dataset
from .MovingMNIST.MovingMNIST import MovingMNIST as MM
import os, numpy as np, torch

class MovingMNIST(Dataset):

    def __init__(self, dataPrefix=None, **kwargs):

        dataPrefix = dataPrefix or os.path.dirname(__file__)+'/MovingMNIST'
        super(MovingMNIST, self).__init__(dataPrefix, **kwargs)
        train_set = MM(root="./MovingMNIST/moving_mnist", train=True, download=True)
        test_set = MM(root="./MovingMNIST/moving_mnist", train=False)
        train_data = train_set.train_data
        test_data = test_set.test_data

        self.data = torch.cat([train_data, test_data], axis=0)
        self.data_properties = [{}]*self.data.size(0)
        self.partitions = {'train':np.arange(train_data.shape[0]), 'test':train_data.shape[0] + np.arange(test_data.shape[0])}
        self.files = [None]*len(self.data)
        self.hash = {None:list(range(self.data.shape[0]))}
