"""

 Import toolbox       : Import utilities

 This file contains lots of useful utilities for dataset import.

 Author               : Philippe Esling
                        <esling@ircam.fr>

"""

import numpy as np, torch
import os
import re
import copy
import pdb

# Package-specific imports
# from .data_audio import DatasetAudio
from .data_metadata import metadata_callbacks

def checklist(item, n=1, rev=False, fill=False, copy=False):
    if not issubclass(type(item), list):
        if copy:
            item = [copy.deepcopy(item) for i in range(n)]
        else:
            item = [item]*n
    if rev:
        item = list(reversed(item))
    if fill and len(item) < n:
        item = item + [None]*(n - len(item))
    return item

def checktuple(item, n=1, rev=False):
    if not issubclass(type(item), tuple):
        item = (item,)*n
    if rev:
        item = list(reversed(item))
    return item

def dyn_expand(tensor, target_size):
    for i, t in enumerate(target_size):
        if tensor.size(i) > t:
            if i > 0:
                index = tuple([slice(None, None, None)]*(i) + [slice(0, t)])
            else:
                index = slice(0, t)
            tensor = tensor.__getitem__(index)
        elif tensor.size(i) < t:
            zeros_shape = list(target_size[:(i+1)]) + [int(t) for t in tensor.shape[(i+1):]]
            zeros_shape[i] = int(target_size[i] - tensor.size(i))
            zeros = torch.zeros(*tuple(zeros_shape))
            tensor = torch.cat([tensor, zeros], dim=i)
    return tensor

def dyn_collate(tensor_list, mode="max"):
    if isinstance(tensor_list[0], list):
        out = [None]*len(tensor_list[0])
        for i in range(len(out)):
            out[i] = dyn_collate([t[i] for t in tensor_list])
        return out
    sizes = torch.Tensor([t.shape for t in tensor_list]).int()
    if mode == "min":
        target_size = [torch.min(sizes[:, i]) for i in range(sizes.size(1))]
    elif mode == "max":
        target_size = [torch.max(sizes[:, i]) for i in range(sizes.size(1))]
    else:
        raise ValueError("mode %s not recognized"%mode)
    target_size = [int(t) for t in target_size]
    for i, t in enumerate(tensor_list):
        tensor_list[i] = dyn_expand(t, target_size)
    return torch.stack(tensor_list, dim=0)

def window_data(chunk, window=None, window_overlap=None, dim=0):
    n_windows = (chunk.shape[dim] - window) // window_overlap
    chunk_list = []
    if n_windows >= 0:
        for i in range(n_windows + 1):
            chunk_list.append(torch.index_select(chunk, dim, torch.arange(i * window_overlap, i * window_overlap + window)).unsqueeze(dim))
    else:
        chunk_list = [dyn_expand(chunk), (*chunk.shape[:dim], 1, window)]
    if len(chunk_list) > 2:
        return torch.cat(chunk_list, dim)
    else:
        return chunk_list[0]


def overlap_add(chunk, window, window_overlap, dim=0, mode="overlap_add", window_fn = None):
    target_size = chunk.shape[:dim] + (chunk.size(dim)*window_overlap + window,)
    tensor = torch.zeros(target_size, device=chunk.device, requires_grad=chunk.requires_grad)
    for j in range(chunk.size(dim)):
        chunk_to_add = chunk[..., j, :]
        if window_fn is not None:
            chunk_to_add = window_fn(window)*chunk_to_add
        if mode=="overlap_add":
            tensor[..., slice(j*window_overlap, j*window_overlap+window)] += chunk_to_add
        else:
            tensor[..., slice(j * window_overlap, (j + 1) * window_overlap)] += chunk_to_add
    return tensor





def retrieve_tasks_from_path(metadata_path):
    files = list(filter(lambda x: os.path.isdir(metadata_path + '/' + x), os.listdir(metadata_path)))
    for i in reversed(range(len(files))):
        if not os.path.isfile(metadata_path + '/' + files[i] + '/metadata.txt'):
            del files[i]
    return files

def mkdir(path):
    """ Create a directory """
    assert (os.popen('mkdir -p %s' % path), 'could not create directory')


"""
###################################
# A real resampling function for time series
###################################
function tensorResampling(data, destSize, type)
  # Set the type of kernel
  local type = type or 'gaussian'
  # Check properties of input data
  if data:dim() == 1 then
    data:resize(1, data:dim(1));
  end
  # Original size of input
  local inSize = data:size(2);
  # Construct a temporal convolution object
  local interpolator = nn.TemporalConvolution(inSize, destSize, 1, 1);
  # Zero-out the whole weights
  interpolator.weight:zeros(destSize, inSize);
  # Lay down a set of kernels
  for i = 1,destSize do
    if type == 'gaussian' then
      interpolator.weight[i] = image.gaussian1D(inSize, (1 / inSize), 1, true, i / destSize);
    else
      local kernSize = math.abs(inSize - destSize) * 3;
      # No handling of boundaries right now
      for j = math.max({i-kernSize, 1}),math.min({i+kernSize,destSize}) do
        # Current position in kernel
        local relIdx = (j - i) / kernSize;
        if type == 'bilinear' then
          interpolator.weight[i][j] = 1 - math.abs(relIdx);
        elseif type == 'hermite' then
          interpolator.weight[i][j] = (2 * (math.abs(relIdx) ^ 3)) - (3 * (math.abs(relIdx) ^ 2)) + 1;
        elseif type == 'lanczos' then
          interpolator.weight[i][j] = (2 * (math.abs(relIdx) ^ 3)) - (3 * (math.abs(relIdx) ^ 2)) + 1;
        end
      end
    end
  end
  # print(interpolator.weight);
  return interpolator:forward(data);
end

###################################
# Return the table of unique occurences (and hash values)
###################################
function tableToLabels(table)
  local hash = {}
  local res = {}
  local labels = {};
  for k,v in pairs(table) do
    if (not hash[v]) then
      res[#res+1] = v # you could print here instead of saving to result table if you wanted
      hash[v] = #res;
    end
    labels[k] = hash[v];
  end
  res[#res + 1] = '(none)';
  return labels, res;
end

###################################
# Retrieve unique values and index set from a table
###################################
function uniqueTable(table)
  local hash = {}
  local res = {}
  for k,v in pairs(table) do
    if (not hash[v]) then
      res[#res+1] = v
      hash[v] = {};
      hash[v][1] = k
    else
      hash[v][#hash[v]+1] = k;
    end
  end
  finalRes = {}; finalIDx = {}; i = 1;
  for k,v in pairs(hash) do finalRes[i] = k; finalIDx[i] = torch.Tensor(hash[k]):long(); i = i + 1; end
  return finalRes, finalIDx;
end

###################################
# Retrieve unique values of a tensor
###################################
function uniqueTensor(table)
  local hash = {}
  local res = {}
  for i = 1,table:size(1) do
    if (not hash[table[i]]) then
      res[#res+1] = table[i] # you could print here instead of saving to result table if you wanted
      hash[table[i]] = true
    end
  end
  return res;
end

###################################
# Perform a deep clone of a given object (table with sub-fields as tensors)
###################################
function deepcopy(object)  # http://lua-users.org/wiki/CopyTable
  local lookup_table = {}
  local function _copy(object)
    if type(object) ~= "table" then
      return object
    elseif lookup_table[object] then
      return lookup_table[object]
    end
    local new_table = {}
    lookup_table[object] = new_table
    for index, value in pairs(object) do
      new_table[_copy(index)] = _copy(value)
    end
    return setmetatable(new_table, getmetatable(object))
  end
  return _copy(object)
end

function round(x) return math.floor(x + 0.5) end

###################################
# Create a set of classes labels from a vector
###################################
function createClasses(valuesVector, nbClasses)
  # Create a linear split
  local yVals = torch.linspace(valuesVector:min(), valuesVector:max(), nbClasses + 1)
  local classResult = torch.ones(valuesVector:size(1)):mul(nbClasses);
  local classCounts = torch.zeros(nbClasses);
  # Find corresponding classes
  for i = 1,valuesVector:size(1) do
    for j = 1,yVals:size(1) do
      if (valuesVector[i] < yVals[j]) then
        classResult[i] = j - 1;
        classCounts[j - 1] = classCounts[j - 1] + 1;
        break;
      end
    end
  end
  validClasses = {}; k = 1;
  # Now prune the class (remove empty classes)
  for j = 1,classCounts:size(1) do
    if (classCounts[j] > 0) then validClasses[j] = k; k = k + 1; end
  end
  # Replace classes after pruning
  for i = 1,classResult:size(1) do classResult[i] = validClasses[classResult[i]]; end
  return classResult;
end

###################################
# Perform the MD5 checksum of an object
###################################
function md5(obj)
   local str = torch.serialize(obj)
   return md5.sumhexa(str)
end

# Function to recursively list files of given type
function listFiles(curDir)
  local filesList = {};
  local function listRecurse(baseDir)
    for entity in lfs.dir(baseDir) do
      if entity ~= "." and entity ~= ".." and string.sub(entity,1,1) ~= "." then
        local fullPath = baseDir + '/' .. entity
        # Check the type of the entry
        local mode=lfs.attributes(fullPath,"mode")
        if mode ~= "directory" then
          local insertID = 1;
          for i = 1,#filesList do
            if entity < filesList[i].name then break; end
            insertID = insertID + 1;
          end
          table.insert(filesList, insertID, {});
          filesList[insertID].path = fullPath;
          filesList[insertID].name = entity;
        else
          listRecurse(fullPath);
        end
      end
    end
    return filesList;
  end
  return listRecurse(curDir);
end

function getKeys(table)
  local resTable, curID = {}, 1; 
  for k, v in pairs(table) do
    resTable[curID] = k;
    curID = curID + 1;
  end
  return resTable;
end

"""


# Function to recursively list directories of given type
def listDirectories(baseDir):
    filesList = []
    insertID = 0
    for name in os.listdir(baseDir):
        path = os.path.join(baseDir, name)
        if os.path.isdir(path):
            filesList.append({})
            filesList[insertID]["path"] = path;
            filesList[insertID]["name"] = name;
            insertID = insertID + 1
    return filesList;


def getAudioFileProperty(fileName):
    fileProperties = {};
    fileProperties["channels"] = 0;
    fileProperties["rate"] = 0;
    fileProperties["size"] = 0;
    fileProperties["type"] = "";
    fileProperties["duration"] = 0;
    # Get duration of file
    pfile = os.popen('soxi "' + fileName + '"');
    for properties in pfile:
        typeV = properties[:-1].split(": ")
        if (len(typeV) > 1):
            if (typeV[0][:8] == 'Channels'):
                fileProperties["channels"] = float(typeV[1])
            if (typeV[0][:11] == 'Sample Rate'):
                fileProperties["rate"] = float(typeV[1])
            if (typeV[0][:9] == 'File Size'):
                fileProperties["size"] = float(typeV[1][:-1])
            if (typeV[0][:15] == 'Sample Encoding'):
                fileProperties["type"] = typeV[1]
    pfile.close()
    pfile = os.popen('soxi -D "' + fileName + '"')
    for properties in pfile:
        fileProperties["duration"] = float(properties)
    pfile.close()
    return fileProperties


def exportMetadataProperties(fID, task, metadata, classes):
    fID.write('#-\n' + task + '\n#-\n')
    if (metadata is None):
        return None
    if (task == 'onset'):
        print("Onset check not implemented.")
    elif (task == 'drum'):
        fID.write('Number of annotated \t : ' + str(len(metadata)) + '\n')
        fID.write('Number of classes \t : ' + str(classes["_length"]) + '\n')
        fID.write('Instance values :\n')
        tmpClasses = np.zeros(classes["_length"])
        curID = 0
        for k, v in classes.items():
            if (k != "_length"):
                nbEx = 0
                for f in range(len(metadata)):
                    if (metadata[f][0]):
                        for g in metadata[f][0]["labels"]:
                            if (g == v):
                                nbEx = nbEx + 1
                tmpClasses[curID] = nbEx
                curID = curID + 1
        if (len(tmpClasses) == 0):
            fID.write('**\n**\n**\n WARNING EMPTY CLASSES **\n**\n**\n**\n')
            return
        fID.write('Min \t : ' + str(np.min(tmpClasses)) + '\n')
        fID.write('Max \t : ' + str(np.max(tmpClasses)) + '\n')
        fID.write('Mean \t : ' + str(np.mean(tmpClasses)) + '\n')
        fID.write('Var \t : ' + str(np.std(tmpClasses)) + '\n')
        tmpAnnote, fullTimes = np.zeros(len(metadata)), np.array([])
        for f in range(len(metadata)):
            if (metadata[f][0]):
                tmpAnnote[f] = metadata[f][0]["time"].shape[0]
                fullTimes = np.concatenate((fullTimes, metadata[f][0]["time"]))
        fID.write('Annotation lengths :\n')
        fID.write('Min \t : ' + str(np.min(tmpAnnote)) + '\n')
        fID.write('Max \t : ' + str(np.max(tmpAnnote)) + '\n')
        fID.write('Mean \t : ' + str(np.mean(tmpAnnote)) + '\n')
    elif (task == 'tempo'):
        fID.write('Number of annotated \t : ' + str(len(metadata)) + '\n')
        tmpTempo = np.array(metadata)
        fID.write('Tempo values :\n')
        fID.write('Min \t : ' + str(np.min(tmpTempo)) + '\n')
        fID.write('Max \t : ' + str(np.max(tmpTempo)) + '\n')
        fID.write('Mean \t : ' + str(np.mean(tmpTempo)) + '\n')
        fID.write('Var \t : ' + str(np.std(tmpTempo)) + '\n')
    elif (task == 'cover'):
        curID, coverTable = 0, [None] * len(metadata)
        for v in metadata:
            coverTable[curID] = len(v)
            curID = curID + 1
        fID.write('Number of annotated \t : ' + str(len(coverTable)) + '\n')
        tmpCover = np.array(coverTable)
        fID.write('Number not found (!) \t : ' + str(np.sum((tmpCover == 0) * 1)) + '\n')
        tmpCover = tmpCover[tmpCover != 0]
        fID.write('Cover properties :\n')
        fID.write('Number found \t : ' + str(tmpCover.shape[0]) + '\n')
        fID.write('Min \t : ' + str(np.min(tmpCover)) + '\n')
        fID.write('Max \t : ' + str(np.max(tmpCover)) + '\n')
        fID.write('Mean \t : ' + str(np.mean(tmpCover)) + '\n')
    elif (task == 'melody'):
        fID.write('Number of annotated \t : ' + str(len(metadata)) + '\n');
        tmpAnnote, fullTimes, fullLabels = np.zeros(len(metadata)), np.array([]), np.array([]);
        for f in range(len(metadata)):
            if (metadata[f]):
                if (len(metadata[f][0]["time"]) > 0):
                    tmpAnnote[f] = metadata[f][0]["time"].shape[0]
                    fullTimes = np.concatenate((fullTimes, metadata[f][0]["time"]))
                    fullLabels = np.concatenate((fullLabels, metadata[f][0]["labels"]))
                else:
                    fID.write('Warning : Empty metadata for ' + str(f) + '\n')
        fID.write('Annotation lengths :\n')
        fID.write('Min \t : ' + str(np.min(tmpAnnote)) + '\n')
        fID.write('Max \t : ' + str(np.max(tmpAnnote)) + '\n')
        fID.write('Mean \t : ' + str(np.mean(tmpAnnote)) + '\n')
        fID.write('Pitch values :\n')
        if (len(fullLabels) > 0):
            fID.write('Min \t : ' + str(np.min(fullLabels)) + '\n')
            fID.write('Max \t : ' + str(np.max(fullLabels)) + '\n')
            fID.write('Mean \t : ' + str(np.mean(fullLabels)) + '\n')
        else:
            fID.write('**\n**\n**\n WARNING EMPTY LABELS **\n**\n**\n**\n')
    else:
        fID.write('Number of annotated \t : ' + str(len(metadata)) + '\n')
        if (classes) and (classes["_length"] > 0):
            fID.write('Number of classes \t : ' + str(classes["_length"]) + '\n')
            fID.write('Instance values :\n')
            tmpClasses = np.zeros(classes["_length"])
            curID = 0
            for k, v in classes.items():
                if (k != "_length"):
                    nbEx = 0
                    for f in range(len(metadata)):
                        if (metadata[f] == v):
                            nbEx = nbEx + 1
                    tmpClasses[curID] = nbEx
                    curID = curID + 1
            fID.write('Min \t : ' + str(np.min(tmpClasses)) + '\n')
            fID.write('Max \t : ' + str(np.max(tmpClasses)) + '\n')
            fID.write('Mean \t : ' + str(np.mean(tmpClasses)) + '\n')
            fID.write('Var \t : ' + str(np.std(tmpClasses)) + '\n')


def testDatasetCollection(path):
    fIDm = open(path + '/datasets-metadata.txt', 'w')
    fIDt = open(path + '/datasets-tasks.txt', 'w')
    fIDt.write("%16s\t" % 'Datasets')
    taskTable, taskID = {}, 0
    print(metadata_callbacks)
    for k, v in (metadata_callbacks.items()):
        if (k != 'default'):
            taskTable[taskID] = k
            taskID = taskID + 1
            tmpKey = k
        if (len(k) > 7):
            tmpKey = k[:7]
            fIDt.write('%s\t' % tmpKey);
    fIDt.write('\n');
    # First list all datasets
    datasetsList = listDirectories(path)
    print('Found datasets :');
    for d in range(len(datasetsList)):
        print('  * ' + datasetsList[d]["name"])
        # Now check which task we found
        taskList = listDirectories(datasetsList[d]["path"] + '/metadata/')
        fIDm.write('***\n***\n' + datasetsList[d]["name"] + '\n***\n***\n\n')
        fIDt.write('%16s\t' % datasetsList[d]["name"])
        # Parse all tasks
        finalTasks = {}
        curTask = 0;
        print('    - Parsing tasks folders.')
        for t in range(len(taskList)):
            if (taskList[t]["name"] != 'raw'):
                print('      o ' + taskList[t]["name"])
                finalTasks[curTask] = taskList[t]["name"]
                curTask = curTask + 1
        wroteTask = np.zeros(len(finalTasks))
        for k in range(len(taskTable)):
            foundID = 0
            for k2 in range(len(finalTasks)):
                if (finalTasks[k2] == taskTable[k]):
                    foundID = 1
                    wroteTask[k2] = 1
                    break
            if (foundID == 1):
                fIDt.write('1')
            else:
                fIDt.write('0')
            fIDt.write('\t')
        fIDt.write('\n')
        for k in range(wroteTask.shape[0]):
            if (wroteTask[k] == 0):
                fIDt.write('ERROR - unfound task : ' + finalTasks[k] + '\n')
        print('    - Importing metadatas.')
        # Create a dataset from the task
        startPath, fileN = os.path.split(datasetsList[d]["path"])
        audioOptions = {"dataDirectory": datasetsList[d]["path"], "dataPrefix": startPath, "tasks": finalTasks};
        audioSet = DatasetAudio(audioOptions)
        audioSet.importMetadataTasks()
        print('    - Storing file properties.')
        fIDm.write('#-\n' + 'File properties' + '\n#-\n')
        # Create a set of overall properties
        setRates, setTypes, setChannels, setDurations = {}, {}, {}, np.zeros(len(audioSet.files))
        # Now check properties of the files
        for f in range(len(audioSet.files)):
            fileProp = getAudioFileProperty(audioSet.files[f]);
            if (setRates.get(fileProp["rate"])):
                setRates[fileProp["rate"]] = setRates[fileProp["rate"]] + 1
            else:
                setRates[fileProp["rate"]] = 1
            if (setTypes.get(fileProp["type"])):
                setTypes[fileProp["type"]] = setTypes[fileProp["type"]] + 1
            else:
                setTypes[fileProp["type"]] = 1
            if (setChannels.get(fileProp["channels"])):
                setChannels[fileProp["channels"]] = setChannels[fileProp["channels"]] + 1;
            else:
                setChannels[fileProp["channels"]] = 1;
            setDurations[f] = fileProp["duration"]
        fIDm.write(' * Rates\n');
        for k, v in setRates.items():
            fIDm.write('    - ' + str(k) + ' \t : ' + str(v) + '\n')
        fIDm.write(' * Types\n')
        for k, v in setTypes.items():
            fIDm.write('    - ' + str(k) + ' \t : ' + str(v) + '\n')
        fIDm.write(' * Channels\n')
        for k, v in setChannels.items():
            fIDm.write('    - ' + str(k) + ' \t : ' + str(v) + '\n')
        fIDm.write(' * Durations\n');
        fIDm.write('    - Minimum \t : ' + str(np.min(setDurations)) + '\n');
        fIDm.write('    - Maximum \t : ' + str(np.max(setDurations)) + '\n');
        fIDm.write('    - Mean \t : ' + str(np.mean(setDurations)) + '\n');
        fIDm.write('    - Variance \t : ' + str(np.std(setDurations)) + '\n');
        fIDm.write('\n');
        print('    - Metadata properties.');
        for t in range(len(finalTasks)):
            exportMetadataProperties(fIDm, finalTasks[t], audioSet.metadata[finalTasks[t]],
                                     audioSet.classes[finalTasks[t]]);
            fIDm.write('\n')
        print('    - Metadata verification.');
        fIDm.write('#-\n' + 'Metadata check (for melody,key,chord,drum and structure)' + '\n#-\n');
        for t in range(len(finalTasks)):
            # Here we will check that the annotations do not exceed the file duration !
            if (finalTasks[t] == 'melody') or (finalTasks[t] == 'keys') or (finalTasks[t] == 'chord') or (
                    finalTasks[t] == 'drum') or (finalTasks[t] == 'structure'):
                curMeta = audioSet.metadata[finalTasks[t]]
                fIDm.write('Task ' + finalTasks[t] + '\n')
                for f in range(len(audioSet.files)):
                    if (finalTasks[t] == 'structure') or (finalTasks[t] == 'keys') or (finalTasks[t] == 'chord') or (
                            finalTasks[t] == 'harmony'):
                        if (not curMeta[f]) or (not curMeta[f][0]) or (len(curMeta[f][0]["timeEnd"]) == 0):
                            fIDm.write('Error : File ' + audioSet.files[f] + ' - Does not have metadata !\n')
                        elif (np.max(curMeta[f][0]["timeEnd"]) > setDurations[f]):
                            fIDm.write('Error : File ' + audioSet.files[f] + '\n')
                            fIDm.write('Duration \t : ' + str(setDurations[f]) + '\n')
                            fIDm.write('Max annote \t : ' + str(np.max(curMeta[f][0]["timeEnd"])) + '\n')
                    else:
                        if (not curMeta[f]) or (not curMeta[f][0]) or (len(curMeta[f][0]["time"]) == 0):
                            fIDm.write('Error : File ' + audioSet.files[f] + ' - Does not have metadata !\n')
                        elif (np.max(curMeta[f][0]["time"]) > setDurations[f]):
                            fIDm.write('Error : File ' + audioSet.files[f] + '\n')
                            fIDm.write('Duration \t : ' + str(setDurations[f]) + '\n')
                            fIDm.write('Max annote \t : ' + str(np.max(curMeta[f][0]["time"])) + '\n')
        fIDm.write('\n')
    fIDm.close()
    fIDt.close()