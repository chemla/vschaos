import torch, pdb, matplotlib.pyplot as plt
from . import AbstractVAE, VanillaVAE, disentangle_hidden_params
from ..modules import HiddenModule, AveragingHiddenModule



class NeuralProcess(VanillaVAE):
    take_sequences = True
    HiddenModuleClass = [AveragingHiddenModule, HiddenModule]
    def __init__(self, input_params, latent_params, embedding_params=None, hidden_params=None, **kwargs):
        assert len(input_params) == 2 # one param for inputs, one param for outputs
        super(NeuralProcess, self).__init__(input_params, latent_params, hidden_params=hidden_params, **kwargs)

    @property
    def input_params(self):
        return self.pinput[1]

    def init_modules(self, input_params, latent_params, hidden_params, *args, **kwargs):
        # Disentangle encoders and decoders specifications in case
        hidden_enc_params, hidden_dec_params = disentangle_hidden_params(hidden_params)
        self.encoders = self.make_encoders(input_params, latent_params, hidden_enc_params, *args, **kwargs)
        self.decoders = self.make_decoders(input_params[1], [[input_params[0], latent_params[0]]], hidden_dec_params, *args, **kwargs)

    def get_decoder_input(self, z, x, sort_dim=None):
        #x_dec = torch.cat([x, x_pred], dim=1)
        if sort_dim is not None:
            x = torch.sort(x, dim=2+sort_dim).values
        z = z.unsqueeze(1).repeat(1, x.shape[1], 1)
        return [[x, z]]

    def forward(self, x, y=None, x_all=None, sort_dim=0, options={}, from_layer=-1, multi_decode=False, *args, **kwargs):
        if self.training:
            assert x_all is not None, "training step requires complete data through x_all keyword"
        else:
            if x_all is None:
                x_all = x

        x = self.format_input_data(x)
        if x_all is not None:
            x_all = self.format_input_data(x_all)

        z_enc = self.encode(x)
        if self.training:
            z_all = self.encode(x_all)
            z_dec = self.get_decoder_input(z_all[0]['out'], x_all[0], sort_dim=None)
        else:
            z_dec = self.get_decoder_input(z_enc[0]['out'], x_all[0], sort_dim=None)

        x_rec = self.decode(z_dec, y=None)
        return self.format_output(z_enc, x_rec)



