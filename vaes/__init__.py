
# -*-coding:utf-8 -*-
 
"""
    The ``vaes`` module
    ========================
 
    This package contains all vae models
 
    :Example:
 
    >>> from models.vaes import VanillaVAE
    >>> VanillaVAE()
 
    Subpackages available
    ---------------------

        * vaes
        * criterions
        * distributions
        * modules
 
    Comments and issues
    ------------------------
        
        None for the moment
 
    Contributors
    ------------------------
        
    * Axel Chemla--Romeu-Santos (chemla@ircam.fr)
 
"""
 
# info
__version__ = "0.1.0"
__author__  = "chemla@ircam.fr"
__date__    = ""

# import sub modules
from .. import utils

from .vae_abstractVAE import AbstractVAE, disentangle_hidden_params
from .vae_vanillaVAE import VanillaVAE
from .vae_vanillaDLGM import VanillaDLGM
from .vae_ladderVAE import LadderVAE
from .vae_mxDLGM import AudioMixtureDLGM
from .vae_recurrentVAE import RVAE, VRNN, ShrubVAE
from .vae_predictiveVAE import PredictiveVAE, PredictiveVRNN, PredictiveShrubVAE
from .vae_adversarial import VanillaGAN, InfoGAN
from .vae_NP import NeuralProcess