import math, torch, torch.nn as nn
from . import init_module
from ..utils import checktuple, flatten_seq_method

norm_hash = {'batch':nn.BatchNorm1d, 'instance':nn.InstanceNorm1d, 'layer':nn.LayerNorm, 'none':None, None:None}

class AttentionLayer(nn.Module):
    def __init__(self, input_dim, output_dim, normalization="layer", dropout=0.1,
                 residual_dropout = None, attention_dropout = None, bias=False, heads=4, preserve_dim=False):
        assert(normalization in norm_hash.keys())
        self.input_dim = input_dim; self.heads = heads
        self.output_dim_heads = output_dim // heads
        self.output_dim = self.output_dim_heads * heads
        self.key = init_module(nn.Linear(self.input_dim, self.output_dim, bias=bias))
        self.value = init_module(nn.Linear(self.input_dim, self.output_dim, bias=bias))
        self.query = init_module(nn.Linear(self.input_dim, self.output_dim, bias=bias))

        residual_dropout = residual_dropout or dropout
        attention_dropout = attention_dropout or dropout
        if residual_dropout:
            self.residual_dropout = nn.Dropout(p=residual_dropout)
        if attention_dropout:
            self.attention_dropout = nn.Dropout(p=attention_dropout)

        self.merge_layer = nn.Linear(output_dim*2, output_dim)
        self.normalization = norm_hash[normalization](output_dim)

    @flatten_seq_method
    def forward(self, key, value=None, query=None):
        if value is None:
            value = key; query = key
        else:
            assert value is not None and query is not None
        residual = query
        key = self.key(key).reshape(key.shape[0], key.shape[1], self.heads, self.output_dim_heads)
        value = self.value(value).reshape(value.shape[0], value.shape[1], self.heads, self.output_dim_heads)
        query = self.query(query).reshape(query.shape[0], query.shape[1], self.heads, self.output_dim_heads)

        key = key.permute(2, 0, 1, 3).contiguous().view(-1, key.shape[1], self.num_hidden_per_attn)
        value = value.permute(2, 0, 1, 3).contiguous().view(-1, value.shape[1], self.num_hidden_per_attn)
        query = query.permute(2, 0, 1, 3).contiguous().view(-1, query.shape[1], self.num_hidden_per_attn)

        attn = torch.bmm(query, key.transpose(1, 2))
        attn = attn / math.sqrt(self.output_dim_heads)
        attn = torch.softmax(attn, dim=-1)
        if hasattr(self, 'attention_dropout'):
            attn = self.attention_dropout(attn)
        # Get Context Vector
        att_output = torch.bmm(attn, value)

        att_output = att_output.view(self.heads, key.shape[1], query.shape[2], self.output_dim_heads)
        att_output = att_output.permute(1, 2, 0, 3).contiguous().view(key.shape[1], query.shape[2], -1)

        # Concatenate context vector with input (most important)
        att_output = torch.cat([residual, att_output], dim=-1)
        result = self.merge_layer(att_output)

        # Residual dropout & connection
        if hasattr(self, "residual_dropout"):
            result = self.residual_dropout(result)
        result = result + residual

        # Layer normalization
        result = self.layer_norm(result)
        return result



