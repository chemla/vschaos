#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 20:09:45 2018

@author: chemla
"""
import pdb
import torch, numpy as np
from .criterion_criterion import Criterion
from ..utils import fromOneHot
import numpy as np, os, pdb
from scipy.stats import norm
from ..monitor.visualize_dimred import MDS

perceptive_flags = {'with_piano':'resources/timbre2020.vs',
                    'no_piano':'resources/timbre2020_np.vs'}

def get_hash(header, task):
    labels = list(sorted(set([k[task] for k in header])))
    hash = {k: [] for k in labels}
    for i, h in enumerate(header):
        hash[h[task]].append(i)
    return hash, labels

def get_valid_instruments(flag="with_piano"):
    assert flag in perceptive_flags.keys(), "flag %s matches no perceptual experiment" % flag
    fullTimbreData = np.load(f"{os.path.dirname(__file__)}/{perceptive_flags[flag]}", allow_pickle=True)
    inst_h, inst_l = get_hash(fullTimbreData['header'], 'instrument')
    return list(inst_l)


def filter_dataset_perceptive(dataset, flag="with_piano", return_complementary=False):
    assert flag in perceptive_flags.keys(), "flag %s matches no perceptual experiment" % flag
    fullTimbreData = np.load(f"{os.path.dirname(__file__)}/{perceptive_flags[flag]}", allow_pickle=True)
    # Names of the pre-extracted set of instruments (all with pairwise rates)
    valid_metadata = fullTimbreData['header']
    for i, meta in enumerate(valid_metadata):
        valid_metadata[i] = {k: dataset.classes[k][str(v)] for k, v in meta.items()}
    required_keys = set(sum([list(v.keys()) for v in valid_metadata], []))
    missing_metadata = set(required_keys).difference(set(dataset.metadata.keys()))
    assert len(missing_metadata) == 0, "missing metadata : %s"%(missing_metadata)
    valid_dyn = [dataset.classes['dynamics'][d] for d in ['pp', 'mf', 'ff']]

    valid_ids = []
    len_dataset = len(dataset.metadata[list(required_keys)[0]])
    for i in range(len_dataset):
        y = {k:int(dataset.metadata[k][i]) for k in ['instrument', 'pitch', 'octave']}
        if y in valid_metadata and (dataset.metadata["dynamics"][i] in valid_dyn):
            valid_ids.append(i)
    if return_complementary:
        missing_ids = list(set(range(len_dataset)).difference(set(valid_ids)))
        valid_dataset = dataset.retrieve(np.array(valid_ids))
        missing_dataset = dataset.retrieve(np.array(missing_ids))
        return valid_dataset, missing_dataset
    else:
        return dataset.retrieve(np.array(valid_ids))

def get_perceptual_centroids(dataset, mds_dims, covariance=True, average_class=None,
                             timbreNormalize=True, flag="with_piano"):

    assert flag in perceptive_flags.keys(), "flag %s matches no perceptual experiment"%flag
    fullTimbreData = np.load(f"{os.path.dirname(__file__)}/{perceptive_flags[flag]}", allow_pickle=True)
    # Names of the pre-extracted set of instruments (all with pairwise rates)
    selectedInstruments = fullTimbreData['header']
    # Full sets of ratings (i, j) = all ratings for instru. i vs. instru. j
    fullMatrix = fullTimbreData['distances']
    prior_params = {}; prior_gauss_params = {}; mds_dynwise = {}
    if average_class is not None:
        average_h, average_l = get_hash(fullTimbreData['header'], average_class)
    for dynamic, detailedMatrix in fullMatrix.items():
        # Final matrices
        nbIns = len(selectedInstruments) if average_class is None else len(average_l)
        meanRatings = np.zeros((nbIns, nbIns))
        gaussMuRatings = np.zeros((nbIns, nbIns))
        gaussStdRatings = np.zeros((nbIns, nbIns))
        nbRatings = np.zeros((nbIns, nbIns))
        # Fit Gaussians for each of the sets of pairwise instruments ratings
        for i in range(nbIns):
            for j in range(i + 1, nbIns):
                nbRatings[i, j] = detailedMatrix.shape[0]
                if average_class is None:
                    currentMatrix = detailedMatrix[:, i, j]
                else:
                    currentMatrix = detailedMatrix[:, average_h[average_l[i]]][...,np.array(average_h[average_l[j]])]
                meanRatings[i, j] = np.mean(currentMatrix)
                # Model the gaussian distribution of ratings
                mu, std = norm.fit(currentMatrix)
                # Fill parameters of the Gaussian
                gaussMuRatings[i, j] = mu
                gaussStdRatings[i, j] = std

        # Create square matrices
        meanRatings += meanRatings.T
        gaussMuRatings += gaussMuRatings.T
        gaussStdRatings += gaussStdRatings.T
        meanRatings = (meanRatings - np.min(meanRatings)) / np.max(meanRatings)
        # Rescale means
        gaussMuRatings = (gaussMuRatings - np.min(gaussMuRatings)) / np.max(gaussMuRatings)
        # Rescale variances
        gaussStdRatings = (gaussStdRatings - np.min(gaussStdRatings)) / np.max(gaussStdRatings)
        variance = np.mean(gaussStdRatings, axis=1)
        if (timbreNormalize):
            variance = ((variance - (np.min(variance)) + 0.01) / np.max(variance)) * 2
        # Compute MDS on Gaussian mean
        seed = np.random.RandomState(seed=3)
        mds = MDS(n_components=len(mds_dims), max_iter=3000, eps=1e-9, random_state=seed, dissimilarity="precomputed",
                  n_jobs=1)
        position = mds.fit(gaussMuRatings).embedding_
        mds_dynwise[dynamic] = mds
        # Store all computations here
        fullTimbreData = {'instruments': selectedInstruments,
                          'ratings': detailedMatrix,
                          'gmean': gaussMuRatings,
                          'gstd': gaussStdRatings,
                          'pos': position,
                          'var': variance}
        np.save('timbre_' + str(len(mds_dims)) + '.npy', fullTimbreData)


        audioTimbreIDs = np.zeros(len(selectedInstruments)).astype('int')

        # Class-dependent means and covariances
        prior_mean = position[audioTimbreIDs]
        prior_std = np.ones((len(selectedInstruments), len(mds_dims)))
        if (covariance == 1):
            prior_std = prior_std * variance[audioTimbreIDs, np.newaxis]
        prior_params[dynamic] = (prior_mean, prior_std)
        # Same for full Gaussian
        prior_gauss_params[dynamic] = (gaussMuRatings, gaussStdRatings)

    if dataset is None:
        labels = None
    else:
        if average_class is None:
            labels = [{k: dataset.classes[k][str(v)] for k, v in m_tmp.items()} for m_tmp in selectedInstruments]
        else:
            labels = [{average_class: dataset.classes[average_class][i]} for i in average_l]
    return labels, prior_params, prior_gauss_params


def get_distance_matrix(matrix, labels, y, default=None):
    target_labels = list(labels[0].keys())
    len_metadata = len(y[target_labels[0]])
    dist_matrix = torch.zeros(len_metadata, len_metadata)
    for i in range(len_metadata):
        for j in range(i+1, len_metadata):
            current_meta_i = {k: int(y[k][i]) for k in target_labels}
            current_meta_j = {k: int(y[k][j]) for k in target_labels}
            if (current_meta_i in labels) and (current_meta_j in labels):
                dist_matrix[i, j] = matrix[labels.index(current_meta_i), labels.index(current_meta_j)]
            else:
                current_meta_i = {default[0]:current_meta_i[default[0]]}
                current_meta_j = {default[0]:current_meta_j[default[0]]}
                dist_i = default[1].index(current_meta_i); dist_j = default[1].index(current_meta_j)
                dist_matrix[i,j] = default[2][dist_i, dist_j]
    dist_matrix += dist_matrix.t()
    return dist_matrix


class PerceptiveL2Loss(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False, flag="with_piano", target_dims = None, default_task = None):
        super(PerceptiveL2Loss, self).__init__()
        self.target_dims = target_dims or list(range(latent_params['dim']))
        self.target_dims = torch.LongTensor(self.target_dims)

        self.labels, _, self.gauss_params = get_perceptual_centroids(dataset, self.target_dims, flag=flag)

        self.default_labels = None;
        self.default_gauss_params = None
        self.default_task = default_task
        if self.default_task is not None:
            self.default_labels, _, self.default_gauss_params = get_perceptual_centroids(dataset, self.target_dims,
                                                                                         average_class=default_task)
        self.dyn_hash = {v:k for k, v in dataset.classes['dynamics'].items()}
        self.normalize = normalize


    def get_target_dist(self, y):
        current_dyn = self.dyn_hash[y['dynamics'][0].item()]
        if self.default_task is not None:
            default_mean = [self.default_task, self.default_labels, self.default_gauss_params[current_dyn][0]]
        else:
            default_mean = None
        targetDistMat = get_distance_matrix(self.gauss_params[current_dyn][0], self.labels, y, default=default_mean)
        targetDistMat.requires_grad_(False)
        targetDistMat /= targetDistMat.max()
        return targetDistMat

    def get_latent_dist(self, z):
        # Compute the dot product
        r = torch.mm(z.index_select(-1, self.target_dims), z.index_select(-1, self.target_dims).t())
        # Get the diagonal elements
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2 * r + (1e-9)
        actualDistMat = actualDistMat.sqrt()
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        return actualDistMat

    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        assert not y is None
        z = out['z_enc'][layer]
        if len(z.shape) == 3:
            z = z[:, 0]
        assert y['dynamics'].unique().shape[0] == 1
        targetDistMat = self.get_target_dist(y)
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute difference to the desired distances
        actualDistMat = self.get_latent_dist(z)
        res = torch.sqrt(torch.sum((actualDistMat - targetDistMat)**2))
        return res, (res, )

    def get_named_losses(self, losses):
        dict_losses = {'l2_perceptive':losses[0].item()}
        return dict_losses


class PerceptiveGaussianLoss(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False, target_dims=None, default_task=None):
        super(PerceptiveGaussianLoss, self).__init__()
        self.target_dims = target_dims or list(range(latent_params['dim']))
        self.target_dims = torch.LongTensor(self.target_dims)
        self.labels, _, self.gauss_params = get_perceptual_centroids(dataset, self.target_dims)
        dyn_labels = ['pp', 'mf', 'ff']
        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(1, 3)
        # for i, d in enumerate(dyn_labels):
        #     mat = self.gauss_params[d][1]
        #     ax[i].matshow(mat)
        # plt.show()

        self.default_labels = None; self.default_gauss_params = None
        self.default_task = default_task
        if self.default_task is not None:
            self.default_labels, _, self.default_gauss_params = get_perceptual_centroids(dataset, self.target_dims,
                                                                                        average_class=default_task)
        self.dyn_hash = {v: k for k, v in dataset.classes['dynamics'].items()}
        self.normalize = normalize

    def get_target_dist(self, y):
        current_dyn = self.dyn_hash[y['dynamics'][0].item()]
        if self.default_task is not None:
            default_mean = [self.default_task, self.default_labels, self.default_gauss_params[current_dyn][0]]
            default_std = [self.default_task, self.default_labels, self.default_gauss_params[current_dyn][1]]
        else:
            default_mean = default_std = None
        targetMeans = get_distance_matrix(self.gauss_params[current_dyn][0], self.labels, y,
                                          default=default_mean)
        targetStds = get_distance_matrix(self.gauss_params[current_dyn][1], self.labels, y,
                                         default=default_std)
        # targetMeans = self.gauss_params[current_dyn][0][y, :][:, y] + 1e-5
        # targetStds = self.gauss_params[current_dyn][1][y, :][:, y] + 1e-5
        targetMeans.requires_grad_(False)
        targetStds.requires_grad_(False)
        targetDistMat = torch.normal(targetMeans, targetStds)
        targetDistMat = targetDistMat / (targetDistMat.max())
        return targetDistMat

    def get_latent_dist(self, z):
        r = torch.mm(z[:, self.target_dims], z[:, self.target_dims].t())
        # Get the diagonal elements
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2 * r + (1e-7)
        actualDistMat = actualDistMat.sqrt()
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        return actualDistMat

    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        z = out['z_enc'][layer]
        if len(z.shape) == 3:
            z = z[:, 0]
        z = z.index_select(-1, self.target_dims)
        assert y['dynamics'].unique().shape[0] == 1
        targetDistMat = self.get_target_dist(y)
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute the dot product
        actualDistMat = self.get_latent_dist(z)
        # Compute difference to the desired distances
        res = torch.sqrt(torch.sum((actualDistMat - targetDistMat)**2))
        return res, (res,)

    def get_named_losses(self, losses):
        dict_losses = {'gaussian_perceptive':losses[0].item()}
        return dict_losses


class PerceptiveStudent(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False, target_dims = None, default_task = None):
        super(PerceptiveStudent, self).__init__()
        self.target_dims = target_dims or list(range(latent_params['dim']))
        self.target_dims = torch.LongTensor(self.target_dims)
        self.labels, _, self.gauss_params = get_perceptual_centroids(dataset, self.target_dims)

        self.default_labels = None
        self.default_gauss_params = None
        self.default_task = default_task
        if self.default_task is not None:
            self.default_labels, _, self.default_gauss_params = get_perceptual_centroids(dataset, self.target_dims,
                                                                                         average_class=default_task)
        self.dyn_hash = {v: k for k, v in dataset.classes['dynamics'].items()}
        self.normalize = normalize

    def get_target_dist(self, y):
        current_dyn = self.dyn_hash[y['dynamics'][0].item()]
        if self.default_task is not None:
            default_mean = [self.default_task, self.default_labels, self.default_gauss_params[current_dyn][0]]
        else:
            default_mean = None
        targetDistMat = get_distance_matrix(self.gauss_params[current_dyn][0], self.labels, y, default=default_mean)
        targetDistMat.requires_grad_(False)
        targetDistMat = torch.pow((1 + targetDistMat), -1)
        targetDistMat = (targetDistMat / torch.sum(targetDistMat))
        return targetDistMat

    def get_latent_dist(self, z):
        r = torch.mm(z[:, self.target_dims], z[:, self.target_dims].t())
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2 * r + (1e-9)
        # Final fronteer
        actualDistMat = torch.exp(- torch.sqrt(actualDistMat))
        actualDistMat = (actualDistMat / torch.sum(actualDistMat, 0))
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        return actualDistMat

    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        z = out['z_enc'][layer]
        if len(z.shape) == 3:
            z = z[:, 0]
        z = z.index_select(-1, self.target_dims)
        assert y['dynamics'].unique().shape[0] == 1

        #targetDistMat = targetDistMat / torch.max(targetDistMat)

        targetDistMat = self.get_target_dist(y)
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute the dot product
        # Get the diagonal elements

        # Compute difference to the desired distances
        #res = torch.sum(((1 + ((actualDistMat - targetDistMat)**2)) ** -1)) #/ torch.sum(((1 + ((actualDistMat - targetDistMat)**2) ** -1))))
        actualDistMat = self.get_latent_dist(z)
        res = torch.sum((actualDistMat) * torch.log(actualDistMat/targetDistMat))
        return res, (res, )

    def get_named_losses(self, losses):
        dict_losses = {'student_perceptive':losses[0].item()}
        return dict_losses
