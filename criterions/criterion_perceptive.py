#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 20:09:45 2018

@author: chemla
"""
import pdb
import torch, numpy as np
from .criterion_criterion import Criterion
from ..utils import fromOneHot
import numpy as np, os, pdb
from scipy.stats import norm
from ..monitor.visualize_dimred import MDS


equivalenceInstruments = ['Clarinet-Bb', 'Alto-Sax', 'Trumpet-C', 'Violoncello',
                          'French-Horn', 'Oboe', 'Flute', 'English-Horn',
                          'Bassoon', 'Tenor-Trombone', 'Piano', 'Violin']


def get_perceptual_centroids(dataset, mds_dims, timbre_path='resources/timbre.npy', covariance=True, timbreNormalize=True,
                             timbreProcessing=True):
    if (timbreProcessing == True or (not os.path.isfile('timbre_' + str(mds_dims) + '.npy'))):
        fullTimbreData = np.load(f"{os.path.dirname(__file__)}/{timbre_path}", allow_pickle=True)[None][0]
        # Names of the pre-extracted set of instruments (all with pairwise rates)
        selectedInstruments = fullTimbreData['instruments']
        # Full sets of ratings (i, j) = all ratings for instru. i vs. instru. j
        detailedMatrix = fullTimbreData['ratings']
        # Final matrices
        nbIns = len(selectedInstruments)
        meanRatings = np.zeros((nbIns, nbIns))
        gaussMuRatings = np.zeros((nbIns, nbIns))
        gaussStdRatings = np.zeros((nbIns, nbIns))
        nbRatings = np.zeros((nbIns, nbIns))
        # Fit Gaussians for each of the sets of pairwise instruments ratings
        for i in range(nbIns):
            for j in range(i + 1, nbIns):
                nbRatings[i, j] = detailedMatrix[i, j].size
                meanRatings[i, j] = np.mean(detailedMatrix[i, j])
                # Model the gaussian distribution of ratings
                mu, std = norm.fit(detailedMatrix[i, j])
                # Fill parameters of the Gaussian
                gaussMuRatings[i, j] = mu
                gaussStdRatings[i, j] = std
                print("%s vs. %s : mu = %.2f,  std = %.2f" % (selectedInstruments[i], selectedInstruments[j], mu, std))
        # Create square matrices
        meanRatings += meanRatings.T
        gaussMuRatings += gaussMuRatings.T
        gaussStdRatings += gaussStdRatings.T
        meanRatings = (meanRatings - np.min(meanRatings)) / np.max(meanRatings)
        # Rescale means
        gaussMuRatings = (gaussMuRatings - np.min(gaussMuRatings)) / np.max(gaussMuRatings)
        # Rescale variances
        gaussStdRatings = (gaussStdRatings - np.min(gaussStdRatings)) / np.max(gaussStdRatings)
        variance = np.mean(gaussStdRatings, axis=1)
        if (timbreNormalize):
            variance = ((variance - (np.min(variance)) + 0.01) / np.max(variance)) * 2
        # Compute MDS on Gaussian mean
        seed = np.random.RandomState(seed=3)
        mds = MDS(n_components=mds_dims, max_iter=3000, eps=1e-9, random_state=seed, dissimilarity="precomputed",
                  n_jobs=1)
        position = mds.fit(gaussMuRatings).embedding_
        # Store all computations here
        fullTimbreData = {'instruments': selectedInstruments,
                          'ratings': detailedMatrix,
                          'gmean': gaussMuRatings,
                          'gstd': gaussStdRatings,
                          'pos': position,
                          'var': variance}
        np.save('timbre_' + str(mds_dims) + '.npy', fullTimbreData)
    else:
        # Retrieve final data structure
        fullTimbreData = np.load('timbre.npy').item()
        # Names of the pre-extracted set of instruments (all with pairwise rates)
        selectedInstruments = fullTimbreData['instruments']
        # Gaussian modelization of the ratings
        gaussMuRatings = fullTimbreData['gmean']
        gaussStdRatings = fullTimbreData['gstd']
        # MDS modelization of the ratings
        position = fullTimbreData['pos']
        variance = fullTimbreData['var']

    audioTimbreIDs = np.zeros(len(equivalenceInstruments)).astype('int')

    # Parse through the list of instruments
    for k, v in dataset.classes['instrument'].items():
        if (k != '_length'):
            audioTimbreIDs[v] = equivalenceInstruments.index(k)
    # Class-dependent means and covariances
    prior_mean = position[audioTimbreIDs]
    prior_std = np.ones((len(equivalenceInstruments), mds_dims))
    if (covariance == 1):
        prior_std = prior_std * variance[audioTimbreIDs, np.newaxis]
    prior_params = (prior_mean, prior_std)
    # Same for full Gaussian
    prior_gauss_params = (gaussMuRatings, gaussStdRatings)

    return prior_params, prior_gauss_params



class PerceptiveL2Loss(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False):
        super(PerceptiveL2Loss, self).__init__()
        targetDims = latent_params['dim']
        _, (centroids, _) = get_perceptual_centroids(dataset, latent_params['dim'])
        if issubclass(type(centroids), np.ndarray):
            self.centroids = torch.from_numpy(centroids).type('torch.FloatTensor')
        else:
            self.centroids = centroids.type('torch.FloatTensor')
        self.targetDims = np.arange(targetDims)
        self.normalize = normalize
        
    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        assert not y is None
        z = out['z_enc'][layer]
        y = y['instrument']
        if y.dim() == 2:
            y = fromOneHot(y.cpu()); 
        # Create the target distance matrix
        targetDistMat = self.centroids[y, :][:, y]
        targetDistMat.requires_grad_(False)
        targetDistMat /= targetDistMat.max()
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute the dot product
        r = torch.mm(z[:, self.targetDims], z[:, self.targetDims].t())
        # Get the diagonal elements
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2*r + (1e-9)
        actualDistMat = actualDistMat.sqrt()
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        # Compute difference to the desired distances
        res = torch.sqrt(torch.sum((actualDistMat - targetDistMat)**2))
        return res, (res, )
    
    def get_named_losses(self, losses):
        dict_losses = {'l2_perceptive':losses[0].item()}
        return dict_losses

    
    
    
class PerceptiveGaussianLoss(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False):
        super(PerceptiveGaussianLoss, self).__init__()
        targetDims = latent_params['dim']
        _, (latent_means, latent_stds) = get_perceptual_centroids(dataset, targetDims)
        self.latent_means = torch.from_numpy(latent_means).type('torch.FloatTensor') if issubclass(type(latent_means), np.ndarray) else latent_means.type('torch.FloatTensor')
        self.latent_stds = torch.from_numpy(latent_stds).type('torch.FloatTensor') if issubclass(type(latent_stds), np.ndarray) else latent_stds.type('torch.FloatTensor')
        self.targetDims = np.arange(targetDims)
        self.normalize = normalize

    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        z = out['z_enc'][layer]
        y = y['instrument']
        if y.dim() == 2:
            y = fromOneHot(y); 
        # Create the target distance matrix
        targetMeans = self.latent_means[y, :][:, y] + 1e-5
        targetStds = self.latent_stds[y, :][:, y] + 1e-5
        targetMeans.requires_grad_(False); targetStds.requires_grad_(False)
        targetDistMat = torch.normal(targetMeans, targetStds)
        targetDistMat = targetDistMat / (targetDistMat.max())
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute the dot product
        r = torch.mm(z[:, self.targetDims], z[:, self.targetDims].t())
        # Get the diagonal elements
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2*r + (1e-7)
        actualDistMat = actualDistMat.sqrt()
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        # Compute difference to the desired distances
        res = torch.sqrt(torch.sum((actualDistMat - targetDistMat)**2))
        return res, (res, )
    
    def get_named_losses(self, losses):
        dict_losses = {'gaussian_perceptive':losses[0].item()}
        return dict_losses




class PerceptiveStudent(Criterion):
    def __init__(self, latent_params, dataset=None, normalize=False):
        super(PerceptiveStudent, self).__init__()
        targetDims = latent_params['dim']
        _, (centroids, _) = get_perceptual_centroids(dataset, targetDims)
        if issubclass(type(centroids), np.ndarray):
            self.centroids = torch.from_numpy(centroids).type('torch.FloatTensor')
        else:
            self.centroids = centroids.type('torch.FloatTensor')
        self.targetDims = np.arange(targetDims)
        self.normalize = normalize
        
    def loss(self, model, out, y=None, layer=0, *args, **kwargs):
        z = out['z_enc'][layer]
        y = y['instrument']
        if y.dim() == 2:
            y = fromOneHot(y); 
        # Create the target distance matrix
        targetDistMat = self.centroids[y, :][:, y]
        targetDistMat.requires_grad_(False)
        targetDistMat = torch.pow((1 + targetDistMat), -1)
        targetDistMat = (targetDistMat / torch.sum(targetDistMat))
        #targetDistMat = targetDistMat / torch.max(targetDistMat)
        if z.is_cuda:
            targetDistMat = targetDistMat.to(z.device)
        # Compute the dot product
        r = torch.mm(z[:, self.targetDims], z[:, self.targetDims].t())
        # Get the diagonal elements
        diag = r.diag().unsqueeze(0)
        diag = diag.expand_as(r)
        # Compute the distance matrix in latent space
        actualDistMat = diag + diag.t() - 2*r + (1e-9)
        # Final fronteer
        actualDistMat = torch.exp(- torch.sqrt(actualDistMat))
        actualDistMat = (actualDistMat / torch.sum(actualDistMat, 0))
        if (self.normalize):
            actualDistMat = actualDistMat / (actualDistMat.max())
        # Compute difference to the desired distances
        #res = torch.sum(((1 + ((actualDistMat - targetDistMat)**2)) ** -1)) #/ torch.sum(((1 + ((actualDistMat - targetDistMat)**2) ** -1))))
        res = torch.sum((actualDistMat) * torch.log(actualDistMat/targetDistMat))
        return res, (res, )
    
    def get_named_losses(self, losses):
        dict_losses = {'student_perceptive':losses[0].item()}
        return dict_losses
