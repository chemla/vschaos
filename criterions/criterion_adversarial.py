from . import Criterion
import torch, pdb
import torch.nn as nn
from .. import DataParallel
from .. import distributions as dist
from ..utils import checktuple, checklist
from ..modules.modules_bottleneck import MLP
from ..modules.modules_distribution import get_module_from_density
from numpy import cumprod, array


class Adversarial(Criterion):
    module_class = MLP
    def __init__(self, input_params=None, adversarial_params=None, optim_params={}, cuda=None,
                 clip=None, gradient_penalty=False, grad_penalty_weight=10, **kwargs):
        assert input_params
        super(Adversarial, self).__init__()
        self.adversarial_params=adversarial_params
        self.clip = clip # is [clip_low, clip_high] for WassersteinGAN
        self.gradient_penalty = gradient_penalty
        self.grad_penalty_weight = grad_penalty_weight
        self.input_params = input_params
        self.init_modules(input_params, adversarial_params, **kwargs)
        self.init_optimizer(optim_params)
        if cuda is not None:
            if issubclass(type(cuda), list):
                self.cuda(cuda[0])
                if len(cuda) > 1:
                    self = DataParallel(self, cuda, cuda[0])
            else:
                self.cuda(cuda)


    def init_modules(self, input_params, adversarial_params, **kwargs):
        adversarial_params = adversarial_params or {'dim':500, 'nlayers':2}
        self.hidden_module = MLP(input_params, adversarial_params)
        self.discriminator = nn.Linear(checklist(adversarial_params['dim'])[-1], 1)

    def init_optimizer(self, optim_params={}):
        self.optim_params = optim_params
        alg = optim_params.get('optimizer', 'Adam')
        optim_args = optim_params.get('optim_args', {'lr':1e-3})

        optimizer = getattr(torch.optim, alg)([{'params':self.parameters()}], **optim_args)
        self.optimizer = optimizer

    def loss(self, params1=None, params2=None, sample=False, **kwargs):
        assert params1 is not None
        assert params2 is not None


        loss_gen = 0
        if issubclass(type(params1), dist.Distribution):
            z_fake = params1.rsample().float()
        else:
            z_fake = params1.float()
        if issubclass(type(params2), dist.Distribution):
            z_real = params2.rsample().float()
        else:
            z_real = params2.float()

        data_dim = len(checktuple(self.input_params['dim']))
        if len(z_fake.shape) > data_dim + 1:
            z_fake = z_fake.contiguous().view(cumprod(z_fake.shape[:-data_dim])[-1], *z_fake.shape[-data_dim:])
            z_real = z_real.contiguous().view(cumprod(z_real.shape[:-data_dim])[-1], *z_real.shape[-data_dim:])

        device = z_fake.device
        # get generated loss
        d_gen = torch.sigmoid(self.discriminator(self.hidden_module(z_fake)))

        try:
            assert d_gen.min() >= 0 and d_gen.max() <= 1
        except AssertionError:
            pdb.set_trace()
        loss_gen = self.reduce(torch.nn.functional.binary_cross_entropy(d_gen, torch.ones(d_gen.shape, device=device, requires_grad=False), reduction="none"))

        # get discriminative loss
        d_real = torch.sigmoid(self.discriminator(self.hidden_module(z_real)))
        #print('d_real : ', d_real.min(), d_real.max())
        try:
            assert d_real.min() >= 0 and d_real.max() <= 1
        except AssertionError:
            pdb.set_trace()

        d_fake = torch.sigmoid(self.discriminator(self.hidden_module(z_fake.detach())))
        #print('d_fake : ', d_fake.min(), d_fake.max())
        try:
            assert d_fake.min() >= 0 and d_fake.max() <= 1
        except AssertionError:
            pdb.set_trace()

        loss_real = torch.nn.functional.binary_cross_entropy(d_real, torch.ones(d_real.shape, device=device, requires_grad=False), reduction="none")
        loss_fake = torch.nn.functional.binary_cross_entropy(d_fake, torch.zeros(d_fake.shape, device=device, requires_grad=False), reduction="none")

        self.adv_loss = self.reduce((loss_real+loss_fake)/2)
        losses = (loss_gen.cpu().detach().numpy(), self.adv_loss.cpu().detach().numpy())
        if z_fake.requires_grad:
            if self.gradient_penalty:
                with torch.no_grad():
                    interp_factor = torch.FloatTensor(z_real.shape[0], *tuple([1]*len(z_real.shape[1:]))).repeat(1, *z_real.shape[1:])
                    interp_factor.uniform_(0, 1)
                    interp_factor = interp_factor.to(z_real.device)
                self.in_interp = interp_factor * z_real + ((1 - interp_factor)*z_fake)
                self.out_interp = torch.sigmoid(self.discriminator(self.hidden_module(self.in_interp)))
                grad_outputs = torch.ones(self.out_interp.size()).to(self.out_interp.device)
                grad = torch.autograd.grad(outputs = self.out_interp, inputs = self.in_interp,
                                           grad_outputs = grad_outputs, create_graph=True, retain_graph=True)[0]
                norm_dims = tuple(range(len(self.in_interp.shape)))[1:]
                grad_penalty = self.gradient_penalty*((grad.norm(2, dim=norm_dims) - 1 ) ** 2)  
                grad_penalty = grad_penalty.mean()
                self.adv_loss = self.adv_loss + self.grad_penalty_weight * grad_penalty
                losses = (*losses, grad_penalty.cpu().detach().numpy())
            self.adv_loss.backward(retain_graph=True)
        else:
            losses = (*losses, array([0.]))

        return loss_gen, losses

    def get_named_losses(self, losses):
        if issubclass(type(losses[0]), (tuple, list)):
            outs = {}
            for i,l in enumerate(losses):
                if self.gradient_penalty:
                    outs = {**outs, 'gen_loss_%d'%i:l[0], 'adv_loss_%d'%i:l[1], 'grad_penalty_%d'%i:l[2]}
                else:
                    outs = {**outs, 'gen_loss_%d'%i:l[0], 'adv_loss_%d'%i:l[1]}
            return outs
        else:
            if self.gradient_penalty:
                return {'gen_loss':losses[0], 'adv_loss':losses[1], 'grad_penalty':losses[2]}
            else:
                return {'gen_loss':losses[0], 'adv_loss':losses[1]}

    def step(self, *args, retain=False, **kwargs):
        # in case, compute gradient penalty
        self.optimizer.step()
        self.optimizer.zero_grad()

        if self.clip:
            for param in self.parameters():
                param.data.clamp_(self.clip[0], self.clip[1])

        if not retain:
            self.out_interp = None
            self.in_interp = None
            self.adv_loss = None


# Here we implement InfoGAN as a Adversarial module with additional latent output.
# The hidden_module of the Adversarial discriminator can be shared with a VAE's one with the encoder keyword.
# This way, the encoding part is regularized throught InfoGAN even if the KLD's beta is 0, and the encode function
# of the Vanilla VAE still works

class AdversarialInfo(Adversarial):

    def __init__(self, input_params, latent_params, encoder=None, info_weighting=0.1, **kwargs):
        assert input_params, "ALI needs input parameters"
        assert latent_params, "ALI needs latent parameters"
        super(AdversarialInfo, self).__init__(input_params, latent_params=latent_params, encoder=encoder, **kwargs)
        self.latent_params = latent_params
        self.info_weighting = info_weighting

    def init_modules(self, input_params, adversarial_params, encoder=None, **kwargs):
        if encoder is None:
            adversarial_params = adversarial_params or {'dim':500, 'nlayers':2}
            self.hidden_module = MLP(input_params, adversarial_params)
            self.out_module = get_module_from_density(self.latent_params)
            out_hidden_dim = adversarial_params['dim']
        else:
            #TODO deal with multi-output
            self.hidden_module = encoder.hidden_modules
            self.out_module = encoder.out_modules[0]
            out_hidden_dim = checklist(self.hidden_module.phidden['dim'])[-1]
        self.discriminator = nn.Linear(out_hidden_dim, 1)


    def loss(self, model = None, out = None, target = None, epoch = None, beta=None, warmup=None, *args, **kwargs):
        assert not model is None and not out is None and not target is None, "ELBO loss needs a model, an output and an input"

        #pdb.set_trace()

        z_real = out['z_enc'][-1]
        x_fake = out['x_params'].rsample().float()
        device = z_real.device
        x_real = target.float().to(device)

        # get generated loss
        out_hidden = self.hidden_module(x_fake)
        d_gen = torch.sigmoid(self.discriminator(out_hidden))

        try:
            assert d_gen.min() >= 0 and d_gen.max() <= 1
        except AssertionError:
            pdb.set_trace()

        loss_gen_out = self.reduce(torch.nn.functional.binary_cross_entropy(d_gen, torch.ones(d_gen.shape, device=device)), reduction="none")
        loss_info = self.info_weighting*self.reduce(self.out_module(out_hidden).log_prob(z_real), reduction="mean")
        loss_gen = loss_gen_out + loss_info

        # get discriminative loss
        d_real = torch.sigmoid(self.discriminator(self.hidden_module([x_real])))
        #print('d_real : ', d_real.min(), d_real.max())
        try:
            assert d_real.min() >= 0 and d_real.max() <= 1
        except AssertionError:
            pdb.set_trace()

        d_fake = torch.sigmoid(self.discriminator(self.hidden_module(x_fake.detach())))
        #print('d_fake : ', d_fake.min(), d_fake.max())
        try:
            assert d_fake.min() >= 0 and d_fake.max() <= 1
        except AssertionError:
            pdb.set_trace()

        loss_real = torch.nn.functional.binary_cross_entropy(d_real, torch.ones(d_real.shape, device=device), reduction="none")
        loss_fake = torch.nn.functional.binary_cross_entropy(d_fake, torch.zeros(d_fake.shape, device=device), reduction="none")
        self.adv_loss = self.reduce((loss_real+loss_fake)/2)
        if self.gradient_penalty:
            with torch.no_grad():
                interp_factor = torch.FloatTensor(x_real.shape[0], *tuple([1]*len(x_real.shape[1:]))).repeat(1, *x_real.shape[1:])
                interp_factor.uniform_(0, 1)
                interp_factor = interp_factor.to(x_real.device)
            self.in_interp = interp_factor * x_real + ((1 - interp_factor)*x_fake)
            self.out_interp = torch.sigmoid(self.discriminator(self.hidden_module(self.in_interp)))

        return loss_gen, (loss_gen_out.cpu().detach().numpy(), loss_info.cpu().detach().numpy(), self.adv_loss.cpu().detach().numpy())

    def get_named_losses(self, losses):
        if issubclass(type(losses[0]), (tuple, list)):
            outs = {}
            for i, l in enumerate(losses):
                outs = {**outs, 'gen_loss_%d' % i: l[0], 'info_loss_%d'%i: l[1], 'adv_loss_%d' % i: l[2]}
            return outs
        else:
            return {'gen_loss': losses[0], 'info_loss':losses[1],  'adv_loss': losses[2]}


# Here, Adversarially Learned Inference (ALI) is inheriting the classic adversarial loss, except that
#   does not perform discrimination on the same data (hence, only init and forward are modified)
class ALI(Adversarial):

    def __init__(self, input_params=None, latent_params=None, **kwargs):
        assert input_params, "ALI needs input parameters"
        assert latent_params, "ALI needs latent parameters"
        super(ALI, self).__init__([input_params, latent_params], **kwargs)
        self.latent_params = latent_params


    def loss(self, model = None, out = None, target = None, epoch = None, beta=None, warmup=None, *args, **kwargs):
        assert not model is None and not out is None and not target is None, "ELBO loss needs a model, an output and an input"

        loss_gen = 0
        #pdb.set_trace()

        z_real = out['z_params_enc'][-1].rsample().float()
        device = z_real.device
        x_real = target.float().to(device)

        z_fake = self.latent_params.get('prior', dist.priors.get_default_distribution(self.latent_params['dist'],z_real.shape)).rsample().float().to(device)
        z_fake.requires_grad = True
        x_fake = out['x_params'].rsample().float()

        # get generated loss
        d_gen = torch.sigmoid(self.discriminator(self.hidden_module([x_fake, z_fake])))

        try:
            assert d_gen.min() >= 0 and d_gen.max() <= 1
        except AssertionError:
            pdb.set_trace()

        loss_gen = self.reduce(torch.nn.functional.binary_cross_entropy(d_gen, torch.ones(d_gen.shape, device=device), reduction="none"))

        # get discriminative loss
        d_real = torch.sigmoid(self.discriminator(self.hidden_module([x_real, z_real])))
        #print('d_real : ', d_real.min(), d_real.max())
        try:
            assert d_real.min() >= 0 and d_real.max() <= 1
        except AssertionError:
            pdb.set_trace()

        d_fake = torch.sigmoid(self.discriminator(self.hidden_module([x_fake.detach(), z_fake.detach()])))
        #print('d_fake : ', d_fake.min(), d_fake.max())
        try:
            assert d_fake.min() >= 0 and d_fake.max() <= 1
        except AssertionError:
            pdb.set_trace()

        loss_real = torch.nn.functional.binary_cross_entropy(d_real, torch.ones(d_real.shape, device=device), reduction="none")
        loss_fake = torch.nn.functional.binary_cross_entropy(d_fake, torch.zeros(d_fake.shape, device=device), reduction="none")

        self.adv_loss = self.reduce((loss_real+loss_fake)/2)
        if z_real.requires_grad:
            if self.gradient_penalty:
                raise NotImplementedError
                with torch.no_grad():
                    interp_factor = torch.FloatTensor(z_real.shape[0], *tuple([1]*len(z_real.shape[1:]))).repeat(1, *z_real.shape[1:])
                    interp_factor.uniform_(0, 1)
                    interp_factor = interp_factor.to(z_real.device)
                self.in_interp = interp_factor * z_real + ((1 - interp_factor)*z_fake)
                self.out_interp = torch.sigmoid(self.discriminator(self.hidden_module(self.in_interp)))
            self.adv_loss.backward(retain_graph=True)

        return loss_gen, (loss_gen.cpu().detach().numpy(), self.adv_loss.cpu().detach().numpy())

